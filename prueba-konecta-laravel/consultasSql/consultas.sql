
/***saber el producto con máximo stock***/
    SELECT nombre, stock
    FROM producto
    ORDER BY stock DESC LIMIT 1;

/***saber el producto mas vendido***/
    SELECT producto.nombre, SUM(venta_producto.cantidad) as cantidad
    FROM venta_producto
    JOIN producto ON venta_producto.id_producto = producto.id
    GROUP BY producto.id
    ORDER BY SUM(venta_producto.cantidad) DESC LIMIT 1;
