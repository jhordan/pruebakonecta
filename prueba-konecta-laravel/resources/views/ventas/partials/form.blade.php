<div class="box-body">
    @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    @include('partials.errors')
    <div class="row">
        @csrf
        <div class="col-md-12">
            <h4 class=""><i class="fa fa-book"></i> Informacion Creación Venta Producto</h4>
        </div>
        <div class="col-md-3">
            {{ Form::label('cantidad', 'Cantidad', ['class' => 'control-label']) }}
            {!! Form::number('cantidad', $sale['cantidad'] ?? null,[
                'label' => 'cantidad',
                'class' => '',
                'ph' => '1000',
                'required'
            ])!!}
        </div>
        <div class="col-md-3">
            {{ Form::label('producto', 'Producto', ['class' => 'control-label']) }}
            {!! Form::select('id_producto', $producto  ?? [],
                $sale['id_producto'] ?? null,
                ['label'=>'id_producto','class'=> 'select2', 'style'=>'width: 200%', 'required'])
            !!}
        </div>
    </div>
    </div>
    <br>
</div>
