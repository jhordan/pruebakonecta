<div class="box-body">
    @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h4 class=""><i class="fa fa-book"></i> Importar Archivo Ventas</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
        <div class="col-md-4">
            {{ Form::label('fecha', 'Fecha Importación', ['class' => 'control-label']) }}
            {!! Form::date('fecha','',[
                'label' => 'fecha',
                'class' => '',
                'ph' => '10',
                'required' => 'required',
            ])!!}
        </div>
        <div class="form-group">
            {{ Form::label('archivo', 'Archivo', ['class' => 'control-label']) }}
            <div class="col-md-3">
                <input type="file" name="archivo" />
                <input type="hidden" name="hidden_image" value=""/>
            </div>
        </div>
    </div>
</div>
<div class="overlay loading" style="display:none;">
    <i class="fa fa-refresh fa-spin"></i>
</div>
