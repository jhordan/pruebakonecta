@extends('dash.index')
@section('content')
<section class="content">
	<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <h3 class="box-title">Listado de Ventas</h3>
        </div>
        <div class="box-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table id="sales" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad en Ventas</th>
                        <th>Fecha Venta</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sales as  $sale)
                    <tr>
                        <td>{{  $sale->Productos->nombre . '-'.$sale->Productos->referencia}}</td>
                        <td>{{  $sale->cantidad}}</td>
                        <td>{{  $sale->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad en Ventas</th>
                        <th>Fecha Venta</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div style="display: flex">
        <div class="box-footer">
            <div class="col-md-6" style="width: 300px;">
                <a href="{!! URL('sale/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo Venta</a>
            </div>
        </div>
        <div class="box-footer">
            <div class="col-md-6" style="width: 330px;">
                <a href="{!! URL('sale/export/Viewexcel') !!}" class="btn btn-success"><i class="fa fa-download"></i> Reporte Ventas</a>
            </div>
        </div>
        </div>
    </div>
</section>
@endsection
