<footer class="main-footer">
	<div class="pull-right hidden-xs">
        <b>Version</b> v1.0
    </div>
    <strong>Copyright &copy; {{ date('Y') }} </strong> Jhordan Miller. ALL RIGHTS RESERVED.
</footer>
