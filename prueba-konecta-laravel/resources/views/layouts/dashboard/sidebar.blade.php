<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{asset('build/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">Navegacion Principal</li>
			<li>
				<a href="{{ URL('/home' )}}" class="sub">
					<i class="fa fa-dashboard text-white"></i>
					<span>Dashboard</span>
				</a>
			</li>
			<li>
				<a href="{{ URL('/equipos' )}}" class="sub">
					<i class="fa  fa-users text-white"></i>
					<span>Equipos</span>
				</a>
			</li>
			<li>
				<a href="{{ URL('/jugadores' )}}" class="sub">
					<i class="fa fa-user text-white"></i>
					<span>Jugadores</span>
				</a>
			</li>
			<li>
				<a href="{{ URL('/torneos' )}}" class="sub">
					<i class="fa  fa-soccer-ball-o text-white"></i>
					<span>Sortear Equipos</span>
				</a>
			</li>
			<li>
				<a href="{{ URL('/eliminatorias' )}}" class="sub">
					<i class="fa fa-trophy text-white"></i>
					<span>Jugar Eliminatorias</span>
				</a>
			</li>
		</ul>
	</section>
</aside>
