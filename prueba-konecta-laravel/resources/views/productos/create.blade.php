@extends('dash.index')
@section('content')
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">Productos</h2>
        </div>
        {!! Form::open($ruta) !!}
            @include('productos.partials.form')
        <div class="box-footer">
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary pull-right']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2({
            placeholder: 'Seleccione una opcion'
        });
    });
</script>
@endsection
