@extends('dash.index')
@section('content')
<section class="content">
	<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <h3 class="box-title">Listado de Productos</h3>
        </div>
        <div class="box-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table id="productos" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Acciones</th>
                        <th>Nombre</th>
                        <th>Referencia</th>
                        <th>Precio</th>
                        <th>Peso</th>
                        <th>Categoría</th>
                        <th>Stock</th>
                        <th>Imagen</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productos as  $producto)
                    <tr>
                        <td><a href="{!! URL('producto/'.$producto->id.'/edit') !!}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                        <td>{{ $producto->nombre }}</td>
                        <td>{{  $producto->referencia }}</td>
                        <td>{{  $producto->precio }}</td>
                        <td>{{  $producto->peso }}</td>
                        <td>{{  $producto->categoria }}</td>
                        <td>{{  $producto->stock }}</td>
                        @if ($producto->avatar)
                        <td><img src="{{ URL::to('/') }}/images/{{ $producto->avatar }}" class="img-thumbnail" width="75" /></td>
                            @else
                            <td><img src="{{ URL::to('/') }}/images/avatar/producto.jpg" class="img-thumbnail" width="75" /></td>
                        @endif
                        <td>{{ Form::open(['route' => ['eliminar', $producto->id], 'method' => 'delete']) }}
                            <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Acciones</th>
                        <th>Nombre</th>
                        <th>Referencia</th>
                        <th>Precio</th>
                        <th>Peso</th>
                        <th>Categoría</th>
                        <th>Stock</th>
                        <th>Imagen</th>
                        <th>Eliminar</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div style="display: flex">
            <div class="box-footer">
                <div class="col-md-6" style="width: 330px;">
                    <a href="{!! URL('producto/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo Producto</a>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-6" style="width: 380px;">
                    <a href="{!! URL('producto/export/Viewexcel') !!}" class="btn btn-success"><i class="fa fa-download"></i> Reporte Productos</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
