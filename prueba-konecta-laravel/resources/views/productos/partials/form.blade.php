<div class="box-body">
    @if (session('status'))
        <div class="alert alert-danger">
            {{ session('status') }}
        </div>
    @endif
    @include('partials.errors')
    <div class="row">
        @csrf
        <div class="col-md-12">
            <h4 class=""><i class="fa fa-book"></i> Informacion Creación Producto</h4>
        </div>
        <div class="col-md-3">
            {{ Form::label('nombre', 'Nombre', ['class' => 'control-label']) }}
            {!! Form::text('nombre', $producto['nombre'] ?? null,[
                'label' => 'nombre',
                'class' => '',
                'ph' => 'Galleta',
                'required'
            ])!!}
        </div>
        <div class="col-md-3">
            {{ Form::label('referencia', 'Referencia', ['class' => 'control-label']) }}
            {!! Form::text('referencia', $producto['referencia'] ?? null,[
                'label' => 'referencia',
                'class' => '',
                'ph' => 'referencia grande, kuaker',
                'required'
            ])!!}
        </div>
        <div class="col-md-3">
            {{ Form::label('precio', 'Precio', ['class' => 'control-label']) }}
            {!! Form::number('precio', $producto['precio'] ?? null,[
                'label' => 'precio',
                'class' => '',
                'ph' => '1000',
                'required'
            ])!!}
        </div>
    </div><br><hr>
    <div class="row">
        <div class="col-md-3">
            {{ Form::label('peso', 'Peso', ['class' => 'control-label']) }}
            {!! Form::number('peso', $producto['peso'] ?? null,[
                'label' => 'peso',
                'class' => '',
                'ph' => '200 gr',
                'required'
            ])!!}
        </div>
        <div class="col-md-3">
            {{ Form::label('categoria', 'Categoria', ['class' => 'control-label']) }}
            {!! Form::select('categoria', $categoria  ?? [],
                $producto['categoria'] ?? null,
                ['label'=>'pais','class'=> 'select2', 'style'=>'width: 100%', 'required'])
            !!}
        </div>
        <div class="col-md-3">
            {{ Form::label('stock', 'Cantidad', ['class' => 'control-label']) }}
            {!! Form::number('stock', $producto['stock'] ?? null,[
                'label' => 'stock',
                'class' => '',
                'ph' => '1000',
                'required'
            ])!!}
        </div>
        <div class="form-group">
            {{ Form::label('avatar', 'Avatar', ['class' => 'control-label']) }}
            @isset($producto['avatar'] )
                <div class="col-md-3">
                    <input type="file" name="avatar" />
                    <img src="{{ URL::to('/') }}/images/{{ $producto['avatar'] }}" class="img-thumbnail" width="100" />
                    <input type="hidden" name="hidden_image" value="{{ $producto['avatar'] }}" />
                </div>
                @else
                <div class="col-md-3">
                    <input type="file" name="avatar" />
                    <input type="hidden" name="hidden_image" value=""/>
                </div>
            @endisset
        </div>
    </div>
    </div>
</div>
