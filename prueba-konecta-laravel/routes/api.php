<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['web']], function () {
    Route::resource('producto', ProductoController::class);
    Route::delete('delete/{id}',array('uses' => 'ProductoController@destroy', 'as' => 'eliminar'));
    Route::resource('sale', VentaController::class);
    /****otras rutas */
    Route::get('sale/export/Viewexcel', 'VentaController@export');
    Route::get('producto/export/Viewexcel', 'ProductoController@export');
});


