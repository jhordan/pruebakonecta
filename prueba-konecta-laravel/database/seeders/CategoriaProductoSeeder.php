<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CategoriaProducto;

class CategoriaProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = new CategoriaProducto();
        $categoria->tipo = 1;
        $categoria->nombre = 'LACTEOS';
        $categoria->save();

        $categoria1 = new CategoriaProducto();
        $categoria1->tipo = 2;
        $categoria1->nombre = 'POSTRES';
        $categoria1->save();

        $categoria2 = new CategoriaProducto();
        $categoria2->tipo = 3;
        $categoria2->nombre = 'DULCES';
        $categoria2->save();
    }
}
