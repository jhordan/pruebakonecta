<?php

namespace App\Models;

use App\Models\CategoriaProducto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'producto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'referencia',
        'precio',
        'peso',
        'categoria',
        'stock',
        'avatar'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    /**
     * Relacioon con la tabla Paises.
     *
     * @var array
     */
    public function users()
    {
        return $this->belongsTo(CategoriaProducto::class, 'id_producto', 'id');
    }

    public function getFullNameAttribute()
    {
     return $this->nombre . '' . $this->referencia . ' - ' . 'Cantidad en Stock ('.$this->stock.')';
    }
}
