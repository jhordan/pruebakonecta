<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Producto;

class VentaProducto extends Model
{
    use HasFactory;

    protected $table = 'venta_producto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_producto',
        'cantidad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

     /**
     * Relacioon con la tabla Producto.
     *
     * @var array
     */
    public function Productos()
    {
        return $this->belongsTo(Producto::class, 'id_producto', 'id');
    }
}
