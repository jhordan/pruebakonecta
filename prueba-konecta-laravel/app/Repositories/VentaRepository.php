<?php

namespace App\Repositories;

use App\Models\VentaProducto;

/**
 * VentaRepository Repository.
 *
 * Clase que se utilza para el acceso a los datos
 *
 * @package App
 * @subpackage App\Repositories
 * @author Jhordan Miller<jhojamil92@gmail.com>
 * @version v1.0.0
 */
class VentaRepository
{
    /**
     * venta $model.
     * @var [type]
     */
    private $model;
    /**
     * Constructor de la clase.
     *
     * @param
     */
    public function __construct()
    {
        $this->model = new VentaProducto();
    }
    /**
     * Traer registros para pintar en la tabla venta productos
     *
     * @access public
     * @return object
     */
    public function ajaxDatatableSale(): object
    {
        return $this->model->get();
    }
    /**
     * Guarda los registros en la tabla venta productos
     *
     * @access public
    *  @param collection $data
     * @return object
     */
    public function saveData($data): object
    {
        $sale = $this->model->create($data);
        return $sale;
    }
     /**
     * Actualiza los registros en la tabla venta productos
     *
     * @access public
     * @param collection $data
     * @param integer $id
     * @return bool
     */
    public function update($data, int $id): bool
    {
        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * traer datos de una venta producto espefica
     *
     * @access public
     * @param integer $id
     * @return object
     */
    public function getDateSale(int $id): object
    {
        return $this->model->findOrFail($id);
    }
}
