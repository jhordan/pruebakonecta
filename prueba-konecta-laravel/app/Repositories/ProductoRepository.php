<?php

namespace App\Repositories;

use App\Models\Producto;
use App\Models\VentaProducto;

/**
 * ProductoRepository Repository.
 *
 * Clase que se utilza para el acceso a los datos
 *
 * @package App
 * @subpackage App\Repositories
 * @author Jhordan Miller<jhojamil92@gmail.com>
 * @version v1.0.0
 */
class ProductoRepository
{
    /**
     * Producto $model.
     * @var [type]
     */
    private $model;
    /**
     * Constructor de la clase.
     *
     * @param
     */
    public function __construct()
    {
        $this->model = new Producto();
    }
    /**
     * Traer registros para pintar en la tabla productos
     *
     * @access public
     * @return object
     */
    public function ajaxDatatableProducto(): object
    {
        return $this->model->get();
    }
    /**
     * Guarda los registros en la tabla producto
     *
     * @access public
    *  @param collection $data
     * @return object
     */
    public function saveData($data): object
    {
        $producto = $this->model->create($data);
        return $producto;
    }
     /**
     * Actualiza los registros en la tabla producto
     *
     * @access public
     * @param collection $data
     * @param integer $id
     * @return bool
     */
    public function update($data, int $id): bool
    {
        return $this->model->findOrFail($id)->update($data);
    }

      /**
     * traer datos de un producto espefico
     *
     * @access public
     * @param integer $id
     * @return object
     */
    public function getDateProducto(int $id): object
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Eliminar un registro especifico en la  tabla producto
     *
     * @access public
     * @param collection $data
     * @param integer $id
     * @return bool
     */
    public function deleteProducto(int $id): bool
    {
        /***eliminamos las ventas del producto inicialmente */
        $eliminarProductoVenta = VentaProducto::where('id_producto',$id)->delete();
        /***se elimina el producto */
        $eliminarProducto      = $this->model->findOrFail($id);
        return $eliminarProducto->delete();
    }
}
