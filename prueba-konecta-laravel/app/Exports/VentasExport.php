<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VentasExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Nombre Producto',
            'Cantidad Venta',
            'Fecha Venta'
        ];
    }
    public function collection()
    {
         $ventas = DB::table('venta_producto')
         ->join('producto', 'venta_producto.id_producto', '=', 'producto.id')
         ->select('producto.nombre','venta_producto.cantidad','venta_producto.created_at')->get();
         return $ventas;

    }
}
