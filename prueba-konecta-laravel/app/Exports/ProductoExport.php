<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductoExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Nombre Producto',
            'Referencia Producto',
            'Precio Producto',
            'Peso Producto',
        ];
    }
    public function collection()
    {
         $productos = DB::table('producto')
         ->select('producto.nombre','producto.referencia','producto.precio','producto.peso')->get();
         return $productos;

    }
}
