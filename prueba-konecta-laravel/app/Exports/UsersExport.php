<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Nombre Usuario',
            'Apellidos Usuario',
            'Número Identidicación Usuario',
            'Tipo Usuario',
        ];
    }
    public function collection()
    {
         $ventas = DB::table('users')
         ->join('user_type', 'users.id_type_user', '=', 'user_type.id')
         ->select('users.name','users.surname','users.document_number','user_type.nombre')->get();
         return $ventas;

    }
}
