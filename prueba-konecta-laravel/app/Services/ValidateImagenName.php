<?php
namespace App\Services;

use Illuminate\Http\Request;

/**
 * Clase que valida la imagen en cuanto al tipo, tamaño.
 *
 * Clase Context interface
 *
 * @package App
 * @subpackage App\Services;
 * @author Jhordan Miller<jhojamil92@gmail.com>
 * @param  \Illuminate\Http\Request  $request
 * @return $string
 * @version v1.0.0
 */

class ValidateImagenName
{
    public $bandera;
    public $response;

     /**
     * Constructor de la clase.
     *
     * @param \Illuminate\Http\Request  $request
     */
    public function __construct(Request $request)
    {
        $this->bandera = $request;
    }

     /**
     * Logica para validar la imagen segun la configuracion que se aplique
     *
     * @param  $string  $nombre
     * @return $string
     */
    public function make($nombre): string
    {
        $image_name = $this->bandera->hidden_image;
        $image      = $this->bandera->file($nombre);
        if ($image) {
            $this->bandera->validate([
                $nombre  =>  'required|mimes:jpg,jpeg,bmp,png|max:2048'
            ]);
            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
            $this->response = $image_name;
        } else {
            $this->response = $image_name;
        }
        return $this->response;
    }
}
