<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre'                   => 'required',
            'referencia'               => 'required',
            'precio'                   => 'required|numeric',
            'peso'                     => 'required|numeric',
            'categoria'                => 'required|numeric',
            'stock'                    => 'required|numeric',
            'avatar'                   => 'required|mimes:jpg,jpeg,bmp,png|max:2048'
        ];

        return $rules;
    }

    /**
     * Obtenga los mensajes de error para las reglas de validación definidas.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required'        => 'El campo :attribute es obligatorio',
            'numeric'         => 'El campo :attribute debe ser un número',
            'date'            => 'El campo :attribute no es una fecha válida',
            'date_format'     => 'El campo :attribute no coincide con el formato :format',
            'before_or_equal' => 'El campo :attribute debe ser una fecha anterior o igual a :date',
            'array'           => 'El campo :attribute no es un array',
            'exists'          => 'El campo :attribute no es válido',
            'mimes'           => 'El campo :attribute debe ser jpg, jpeg, bmp, png.',
            'max'             => 'El campo :attribute tiene que ser un tamaño menor a 2.48MB (2048 KB).',
        ];
    }
    /**
     * Obtenga atributos personalizados para los errores del validador.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'nombre'                      => 'Nombre Producto',
            'referencia'                  => 'Referencia Producto',
            'precio'                      => 'Precio Producto',
            'peso'                        => 'Peso Producto',
            'categoria'                   => 'Categoria Producto',
            'stock'                       => 'Stock Producto',
            'avatar'                      => 'Imagen'
        ];
    }
}
