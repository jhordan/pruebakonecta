<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Exports\ProductoExport;
use App\Models\CategoriaProducto;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Services\ValidateImagenName;
use Illuminate\Support\Facades\View;
use App\Repositories\ProductoRepository;
use App\Http\Requests\StoreProductoRequest;
use App\Http\Requests\UpdateProductoRequest;


class ProductoController extends Controller
{

     /**
     * producto Repository.
     * @var [type]
     */
    protected $producto;
    /**
     * Constructor de la clase.
     *
     * @param ProductoRepository $producto
     */
    public function __construct(ProductoRepository $producto)
    {
        $this->producto = $producto;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $producto = $this->producto->ajaxDatatableProducto();
        return View::make('productos.index')->with('productos', $producto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("productos.create", [
            'ruta' => ['id' => 'productos','route' => 'producto.store','enctype' => 'multipart/form-data'],
            'categoria' => CategoriaProducto::pluck('nombre', 'id')->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductoRequest $request)
    {

        DB::beginTransaction();
        try {
            $data     =  $request->all();
            $archivo  = $request->file('avatar');
            if ($archivo) {
                $nombre = rand() . '.' . $archivo->getClientOriginalName();
                $archivo->move('images', $nombre);
                $data['avatar'] = $nombre;
            }
            $producto =  $this->producto->saveData($data);
            DB::commit();

            return redirect('producto')->with('status', 'Producto  Guardado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('producto')->with('status', 'El Producto No Se Pudo Crear!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $producto =  $this->producto->getDateproducto($producto->id);
        return View::make("productos.create", [
            'ruta' => [
                'id' => 'producto',
                'method' => 'PUT',
                'route'  => ['producto.update', $producto], 'autocomplete' => 'on','enctype' => 'multipart/form-data'],
                'producto' => $producto,
                'categoria' => CategoriaProducto::pluck('nombre', 'id')->toArray(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateproductoRequest  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductoRequest $request, producto $producto)
    {
        $data            = $request->all();
        $valdiarImagen   = new ValidateImagenName($request);
        $valdiarImagen   = $valdiarImagen->make('avatar');
        $data['avatar']  = $valdiarImagen;
        DB::beginTransaction();
        try {
            $update = $this->producto->update($data, $producto->id);
            DB::commit();
            return redirect('producto')->with('status', 'Producto fue Actualizado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('producto')->with('status', 'El Producto No Se Pudo Actualizar!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($producto)
    {
        DB::beginTransaction();
        try {
            $delete = $this->producto->deleteProducto($producto);
            DB::commit();
            return redirect('producto')->with('status', 'Producto Eliminado Correctamente!');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('producto')->with('status', 'El Producto No Se Pudo Eliminar!');
        }
    }

       /**
     * export data excel to Producto.
     *
     * @return \Illuminate\Http\Response
     */
    public function export() {
        return Excel::download(new ProductoExport, 'productos.xlsx');
    }
}
