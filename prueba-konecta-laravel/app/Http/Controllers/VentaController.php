<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Exports\VentasExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\VentaRepository;
use App\Http\Requests\StoreSaleRequest;


class VentaController extends Controller
{

     /**
     * venta Repository.
     * @var [type]
     */
    protected $sale;
    /**
     * Constructor de la clase.
     *
     * @param VentaRepository $sale
     */
    public function __construct(VentaRepository $sale)
    {
        $this->sale = $sale;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sale = $this->sale->ajaxDatatableSale();
        return View::make('ventas.index')->with('sales', $sale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make("ventas.create", [
            'ruta' => ['id' => 'sales','route' => 'sale.store','enctype' => 'multipart/form-data'],
            'producto'  => Producto::Where('stock','>',0)->get()->pluck('full_name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSaleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSaleRequest $request)
    {
        DB::beginTransaction();
        try {
            $cantidadActualStock = $this->getCantidadStockProducto($request->id_producto)->stock;
            if($cantidadActualStock == '0' || ($request->cantidad > $cantidadActualStock)){
                return redirect()->back()->with('status', 'El valor de la cantidad no puede ser menor a 0 ni mayor a ' .$cantidadActualStock);
            } else {
                $data       =  $request->all();
                $sale       =  $this->sale->saveData($data);
                $totalActualizarStock    =  $cantidadActualStock - $request->cantidad;
                $actualizarStockProducto = Producto::where('id', $request->id_producto)
                    ->update(['stock' => $totalActualizarStock]);
                DB::commit();

                return redirect('sale')->with('status', 'Venta  Guardada Correctamente!');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('sale')->with('status', 'La Venta No Se Pudo Crear!');
        }
    }

    /**
     * Obtener cantidad actual del stock producto
     *
     * @param  integer $id_producto
     */
    private function getCantidadStockProducto($id_producto) {
        return Producto::Where('id',$id_producto)->first();
    }

    /**
     * export data excel de ventas del producto.
     *
     * @return \Illuminate\Http\Response
     */
    public function export() {
        return Excel::download(new VentasExport, 'ventas.xlsx');
    }
}
