**Prueba de desarrollo.**
Prueba de desarrollo para **Konecta**.

**Paquetes Terceros**
- AdminLTE 2
- maatwebsite/excel

**Pre-requisitos**
- Php 7.3.0 con phpCli habilitado para la ejecución de comando.
- Mysql
- Composer
- Extensión pdo_pgsql habilitada.
- Node & npm

**Instalación**

1. Clonar el repositorio en la carperta del servidor web.
[git clone git@gitlab.com:jhordan/pruebakonecta.git](git@gitlab.com:jhordan/pruebakonecta.git)
[git clone https://gitlab.com/jhordan/pruebakonecta.git](https://gitlab.com/jhordan/pruebakonecta.git)

2. Instalar paquetes.
`composer install`

3. Configurar archivo .env

4. Configure las variables de entorno para base de datos

- `DB_HOST=` Variable de entorno para el host de BD.
- `DB_PORT=` Variable de entorno para el puerto de BD.
- `DB_DATABASE=` Variable de entorno para el nombre de BD.
- `DB_USERNAME=` Variable de entorno para el usuario de BD.
- `DB_PASSWORD=` Variable de entorno para la contraseña de BD.


5. En la raíz del sitio ejecutar.

- php artisan key:generate Genera la llave para el cifrado del proyecto.
- composer install Instala dependencias de PHP
- npm install Instala dependencias de javascript
- npm run dev Genera la llave para el cifrado del proyecto.
- php artisan migrate:refresh --seed Ejecuta migraciones y seeders


**Proceso:**

1. Se pueden crear productos
2. Se pueden crear ventas de estos productos creados 


**Nota:**
1. En la carpeta consultasSql estan los archivos `consultas.sql` que son las consultas de la prueba y `pruebaKonecta.sql` que es un dump de la bd
